package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (statement == null || statement.indexOf(',') != -1 || statement.length() == 0) {
            return null;
        }

        try {
            int indexOfFirstBracket = statement.indexOf('(');
            int indexOfLastBracket = statement.lastIndexOf(')');

            //calculating expression in brackets, if they exist
            if (indexOfFirstBracket != -1 || indexOfLastBracket != -1) {

                if (indexOfLastBracket < indexOfFirstBracket ||
                        (indexOfFirstBracket == -1 && indexOfLastBracket != -1)
                        || (indexOfFirstBracket != -1 && indexOfLastBracket == -1)) {
                    return null;
                }

                String numberInBrackets = evaluate(statement.substring(indexOfFirstBracket + 1, indexOfLastBracket));

                if (numberInBrackets == null) {
                    return null;
                }

                // change initial string to string with evaluate expression in brackets
                String s = statement.substring(0, indexOfFirstBracket + 1)
                        + numberInBrackets
                        + statement.substring(indexOfLastBracket, statement.length());

                statement = s;
                indexOfLastBracket = statement.lastIndexOf(')');
            }

            if (statement.indexOf('+') != -1) {
                return getResultOfOperation(statement, statement.indexOf('+'), "+");
            }

            int indexOfMinus = statement.indexOf('-');
            if (indexOfMinus != -1 && indexOfMinus != 0 && indexOfFirstBracket == -1) {
                return getResultOfOperation(statement, indexOfMinus, "-");
            }

            if (statement.indexOf('*') != -1) {
                return getResultOfOperation(statement, statement.indexOf('*'), "*");
            }

            int indexOfDivision = statement.indexOf('/');
            // if no brackets or if bracket not right away mark '/'
            try {
                if (indexOfDivision != -1 && indexOfFirstBracket != indexOfDivision + 1) {
                    return getResultOfOperation(statement, indexOfDivision, "/");
                    // if bracket right away after mark
                } else if (indexOfDivision != -1 && indexOfFirstBracket == indexOfDivision + 1) {
                    return divisionOperationWithBrackets(statement, indexOfLastBracket, indexOfDivision);
                }
            } catch (ArithmeticException e) {
                return null;
            }

            return statement;
        } catch (NumberFormatException | NullPointerException e) {
            return null;
        }
    }

    private String divisionOperationWithBrackets(String statement, int indexOfLastBracket, int indexOfDivision) {
        String leftNumber = evaluate(statement.substring(0, indexOfDivision));
        String rightNumber = evaluate(statement.substring(indexOfDivision + 2, indexOfLastBracket));
        if (leftNumber.indexOf('.') != -1 || rightNumber.indexOf('.') != -1
                || Integer.parseInt(leftNumber) % Integer.parseInt(rightNumber) != 0) {
            return String.valueOf(Double.parseDouble(leftNumber) / Double.parseDouble(rightNumber));
        }

        return String.valueOf(Integer.parseInt(leftNumber) / Integer.parseInt(rightNumber));
    }


    private String getResultOfOperation(String statement, int indexOfStartOperation, String operationStr) {
        String leftNumber = evaluate(statement.substring(0, indexOfStartOperation));
        String rightNumber = evaluate(statement.substring(indexOfStartOperation + 1, statement.length()));

        if (leftNumber.indexOf('.') != -1 || rightNumber.indexOf('.') != -1
                || ("/".equals(operationStr) && Integer.parseInt(leftNumber) % Integer.parseInt(rightNumber) != 0)) {
            return getStringOfGenericNumbers(operationStr, Double.parseDouble(leftNumber), Double.parseDouble(rightNumber), new Double(3));
        }

        return getStringOfGenericNumbers(operationStr, Double.parseDouble(leftNumber), Double.parseDouble(rightNumber), new Integer(3));
    }

    private <T> String getStringOfGenericNumbers(String operationStr, Double leftNumber, Double rightNumber, T typeValue) {
        Double result = null;
        switch (operationStr) {
            case "+":
                result = leftNumber + rightNumber;
                break;
            case "-":
                result = leftNumber - rightNumber;
                break;
            case "*":
                result = leftNumber * rightNumber;
                break;
            case "/":
                result = leftNumber / rightNumber;
                break;
        }
        if (typeValue instanceof Double) {
            return String.valueOf(result);
        }

        if (typeValue instanceof Integer) {
            String stringHelper = result.toString();
            return String.valueOf(Integer.parseInt(stringHelper.substring(0, stringHelper.indexOf('.'))));
        } else {
            return String.valueOf(result);
        }

    }

}
