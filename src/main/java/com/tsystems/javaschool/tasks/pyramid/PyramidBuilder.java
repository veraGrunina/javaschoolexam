package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int inputSize = inputNumbers.size();
        int maxHeightArray = 0;
        int countInTriangle = 0;

        inputNumbers.get(0);

        while (countInTriangle < inputSize) {
            maxHeightArray++;
            countInTriangle = countInTriangle + maxHeightArray;
            if (countInTriangle < 0) {
                throw new CannotBuildPyramidException();
            }
        }

        if (countInTriangle != inputSize) {
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort((o1, o2) -> {
            if (o1 == null || o2 == null) {
                throw new CannotBuildPyramidException();
            }
            if (o1.intValue() < o2.intValue()) {
                return -1;
            } else if (o1.intValue() > o2.intValue()) {
                return 1;
            } else {
                return 0;
            }
        });

        int[][] pyramid = new int[maxHeightArray][maxHeightArray*2 - 1];

        int countNumbers = 0;

        for (int i = 0; i < pyramid.length; i++) {
            for (int j = pyramid[0].length / 2 - i; j < pyramid[0].length/2 + i + 1; j = j + 2) {
                pyramid[i][j] = inputNumbers.get(countNumbers);
                countNumbers++;
            }
        }

        return pyramid;
    }
}
